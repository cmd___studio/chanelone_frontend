$(function(){

	$(document).ready(function(){
		// DOM ELEMENTS
		const h_outer = $('.header_outer');
		const top_level = $('.top_level');
		const second_level = $('.second_level');
		const logo = top_level.find('.nav-item');	
		const sections = $('.sliding-item');	
		
		//VARIABLES
		let scrolling = false;
		let WH = $(window).height();
		let WW = $(window).width();
		let newHeight;
		let headerH = $('.site-header').height();

		//FUNCTIONS
		function ResizeProportionally(myelem, WH, WW, container_height){
			let current_element_width = myelem.width();
			let current_element_height = myelem.height();
			let current_section_height = container_height+84;
				if((current_element_width*current_section_height)/current_element_height < WW ){
				myelem.css({
				'width':WW,
				'height':current_section_height
				})
				}else{
					myelem.css({
					'width':(current_element_width*current_section_height)/current_element_height,
					'height':current_section_height,
					})
				}	
			     	       
		}
		function getCurrentScroll(){
			return window.pageYOffset || document.documentElement.scrollTop;
		}
		setInterval( function() {
		  if ( scrolling ) {
			 scrolling = false;
		  }
		}, 150 );
		
		//ONLOAD 
		$(window).on('load',function(){
			//set up height of the individual sections		
			sections.each(function(i){		
				$(this).css({
					'height':WH*0.745,
				})
				let section_img = $(this).find('.sliding-hero__picture');
				ResizeProportionally(section_img, WH, WW, WH*0.745)
			});
		})
		
		//ONRESIZE
		$(window).on('resize', function(){
			WH = $(window).height();
			WW = $(window).width();
			sections.each(function(){
				newHeight = (WH*0.745) + 84;
				$(this).css({
						'height':WH*0.745,
				})
				section_img = $(this).find('.sliding-hero__picture');				
				
					let section_img_width = section_img.width();
					let section_img_offset_x = section_img.find('picture').data('offset-landscape-x');
					let value = WW - section_img_width + section_img_offset_x;
				
					ResizeProportionally(section_img, WH, WW, WH*0.745);
				
				if(WW < section_img_width){
					value = WW - section_img_width + section_img_offset_x;
					section_img.css({
										'-webkit-transform': 'translate3d('+ value+'px,0px, 0px)',
										'-o-transform': 'translate3d('+ value+'px,0px, 0px)',
										'-moz-transform': 'translate3d('+ value+'px,0px, 0px)',
										'transform': 'translate3d('+ value+'px,0px, 0px)',
					});
				}else{
					value = WW - section_img_width ;
					section_img.css({
										'-webkit-transform': 'translate3d('+ value+'px,0px, 0px)',
										'-o-transform': 'translate3d('+ value+'px,0px, 0px)',
										'-moz-transform': 'translate3d('+ value+'px,0px, 0px)',
										'transform': 'translate3d('+ value+'px,0px, 0px)',
					});
				}
				
			});
		});
		
		//ONSCROLL
		$(window).scroll(function() {
		  scrolling = true;  
		  let scroll = getCurrentScroll();
		  
		  
		  
		  let newopacity;
		  newopacity = 1-(scroll/120);
		  
		  
		  if(scroll < 120 && scroll > 20){
			  h_outer.css({
										'-webkit-transform': 'translate3d(0px, '+ -scroll/1.7 +'px, 0)',
										'-o-transform': 'translate3d(0px, '+ -scroll/1.7+'px, 0)',
										'-moz-transform': 'translate3d(0px, '+ -scroll/1.7 +'px, 0)',
										'transform': 'translate3d(0px, '+ -scroll/1.7 +'px, 0)',
			  });
			  top_level.css({
										'-webkit-transform': 'translate3d(0px, '+ scroll/1.7 +'px, 0)',
										'-o-transform': 'translate3d(0px, '+ scroll/1.7 +'px, 0)',
										'-moz-transform': 'translate3d(0px, '+ scroll/1.7 +'px, 0)',
										'transform': 'translate3d(0px, '+ scroll/1.7 +'px, 0)',
			  });
			  logo.css({				'-webkit-transform': 'translate3d(0px, '+ - scroll/15 +'px, 0)',
										'-o-transform': 'translate3d(0px, '+ - scroll/15 +'px, 0)',
										'-moz-transform': 'translate3d(0px, '+ - scroll/15 +'px, 0)',
										'transform': 'translate3d(0px, '+ - scroll/15 +'px, 0)',
			  });
			 
			  second_level.css({'opacity':newopacity});
		 		  
		}else{
		  if(scroll < 20){
				 h_outer.css({
											'-webkit-transform':'matrix(1, 0, 0, 1, 0, 0)',
											'-o-transform': 'matrix(1, 0, 0, 1, 0, 0)',
											'-moz-transform': 'matrix(1, 0, 0, 1, 0, 0)',
											'transform': 'matrix(1, 0, 0, 1, 0, 0)',
				  });
				 top_level.css({			'-webkit-transform':'matrix(1, 0, 0, 1, 0, 0)',
											'-o-transform': 'matrix(1, 0, 0, 1, 0, 0)',
											'-moz-transform': 'matrix(1, 0, 0, 1, 0, 0)',
											'transform': 'matrix(1, 0, 0, 1, 0,0)',
				  });
				 second_level.css({'opacity':1});

		   }else{
			setTimeout(function(){
				h_outer.css({
											'-webkit-transform':'matrix(1, 0, 0, 1, 0, -56)',
											'-o-transform': 'matrix(1, 0, 0, 1, 0, -56)',
											'-moz-transform': 'matrix(1, 0, 0, 1, 0, -56)',
											'transform': 'matrix(1, 0, 0, 1, 0, -56)',
				});
				top_level.css({			'-webkit-transform':'matrix(1, 0, 0, 1, 0, 56)',
											'-o-transform': 'matrix(1, 0, 0, 1, 0, 56)',
											'-moz-transform': 'matrix(1, 0, 0, 1, 0, 56)',
											'transform': 'matrix(1, 0, 0, 1, 0, 56)',
				});				 
				}, 100)
		   }
		   }
		
		  sections.each(function(){							
					let picture = $(this).find('.sliding-hero__picture');
					let section_offset = $(this).offset();
					let section_offset_top = section_offset.top;
				
					let myval = scroll/10;
					if($(this).is(':first-child')){
						
					}else{
						if(scroll > 140){
						   if (scroll + WH >= section_offset_top) {						   
							   myval = (-(scroll-section_offset_top)/10);
							   picture.css({
											'-webkit-transform': 'translate3d(0px, '+ -myval +'px, 0)',
											'-o-transform': 'translate3d(0px, '+ -myval+'px, 0)',
											'-moz-transform': 'translate3d(0px, '+ -myval +'px, 0)',
											'transform': 'translate3d(0px, '+ - myval+'px, 0)',
								});
							}
						}
					}
					
					
			});
				
		});
		
		//ONCLICK EVENTS
		$(document).on('click touchstart', '.sliding-item a',function(e){
			e.preventDefault();
			let theli = $(this).parents('li');	
			theli.addClass('remove');
			h_outer.addClass('clicked');
			setTimeout(function(){
				theli.remove();
			}, 900)
		})
		
		$('.menu-toggle').on('click touchstart', function(e){
			e.preventDefault();
			
			$(this).toggleClass('active');
			second_level.fadeIn();
			second_level.css({
				'-webkit-transform': 'translate(100%, 0%) matrix(1, 0, 0, 1,-'+WW+', 0)',
				'-o-transform': 'translate(100%, 0%) matrix(1, 0, 0, 1,-'+WW+', 0)',
				'-moz-transform': 'translate(100%, 0%) matrix(1, 0, 0, 1,-'+WW+', 0)',
				'transform':  'translate(100%, 0%) matrix(1, 0, 0, 1,-'+WW+', 0)',
			
		    })
			
		})
		$('.hideit').on('click touchstart', function(e){
			e.preventDefault();
			
			$(this).toggleClass('active');
			second_level.fadeOut();
			second_level.css({
				'-webkit-transform': 'translate(-100%, 0%)',
				'-o-transform': 'translate(-100%, 0%)',
				'-moz-transform': 'translate(-100%, 0%)',
				'transform':  'translate(-100%, 0%)',
			
		    })
			
		})

	})
})