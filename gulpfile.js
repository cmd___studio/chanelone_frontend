let gulp = require('gulp');
//let {phpMinify} = require('@cedx/gulp-php-minify');
let cleanCSS = require('gulp-clean-css');
let htmlmin = require('gulp-htmlmin');
let sass = require('gulp-sass');
let minify = require('gulp-minify');
 
gulp.task('compress', function() {
  gulp.src(['./dev/js/*.js'])
    .pipe(minify())
    .pipe(gulp.dest('./js/'))
});

gulp.task('default', function() {
  // place code for your default task here
  
});
gulp.task('minify-css', () => {
	  return gulp.src('./css/*.css')
	    .pipe(cleanCSS({debug: true}, (details) => {
	      console.log(`${details.name}: ${details.stats.originalSize}`);
	      console.log(`${details.name}: ${details.stats.minifiedSize}`);
	    }))
	    .pipe(gulp.dest('./css'));
});
gulp.task('htmltask', function(){
  return gulp.src(['./dev/*.html','./dev/*.php'])
      .pipe(htmlmin({
        collapseWhitespace: true,
        ignoreCustomFragments: [ /<%[\s\S]*?%>/, /<\?[=|php]?[\s\S]*?\?>/ ]
      }))
      .pipe(gulp.dest('./'));
});

gulp.task('sass', function () {
  return gulp.src('./dev/sass/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css/'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./dev/sass/*.scss', ['sass']);
});


